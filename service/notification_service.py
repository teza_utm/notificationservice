from model.condition import Condition
from utils.validation import is_action_type_valid
from config.db_connection_config import get_slave, get_master
import datetime
from flask import abort, make_response
from sqlalchemy import or_, and_


def get_all_conditions():
    slave_db = get_slave()
    conditions = slave_db.query(Condition).all()
    conditions_list = []
    for condition in conditions:
        conditions_list.append(condition.to_json())

    return conditions_list


def get_condition_by_id(id):
    slave_db = get_slave()
    condition = slave_db.query(Condition).get(id)

    if not condition:
        abort(make_response('{"message": "Condition does not exist"}', 404))

    return condition.to_json()


def create_condition(body):
    try:
        if is_action_type_valid(body):
            new_condition = Condition(camera_id=body["camera_id"],
                                      name=body["name"],
                                      threshold=body["threshold"],
                                      operator=body["operator"],
                                      actions=body["actions"],
                                      occurrence=body["occurrence"])

            master_db = get_master()
            master_db.add(new_condition)
            master_db.commit()
            return {"message": "Success"}
        else:
            abort(make_response('{"message": "Action type not defined. Check documentation!"}', 400))

    except KeyError:
        abort(make_response('{"message": "Wrong body. Check documentation!"}', 400))


def update_condition(id, body):
    master_db = get_master()
    condition = master_db.query(Condition).get(id)

    if not condition:
        abort(make_response('{"message": "Condition does not exist"}', 404))

    try:
        if not is_action_type_valid(body):
            abort(make_response('{"message": "Action type not defined. Check documentation!"}', 400))

        condition.camera_id = body["camera_id"]
        condition.name = body["name"]
        condition.threshold = body["threshold"]
        condition.operator = body["operator"]
        condition.actions = body["actions"]
        condition.occurrence = body["occurrence"]

        master_db.commit()

        return {"message": "Success"}

    except KeyError:
        abort(make_response('{"message": "Wrong body. Check documentation!"}', 400))


def delete_condition(id):
    master_db = get_master()
    condition = master_db.query(Condition).get(id)
    if not condition:
        abort(make_response('{"message": "Condition does not exist"}', 404))

    master_db.delete(condition)
    master_db.commit()

    return {"message": "Success"}


def get_conditions_by_camera_id(camera_id):
    slave_db = get_slave()
    conditions_list = []
    conditions = slave_db.query.filter(Condition.camera_id == camera_id).all()
    for condition in conditions:
        conditions_list.append(condition)
    return conditions_list


def get_conditions_by_camera_id_threshold(camera_id, people_nr):
    slave_db = get_slave()
    conditions_list = []

    conditions = slave_db.query(Condition)\
        .filter(Condition.camera_id == camera_id)\
        .filter(or_(and_(Condition.threshold < people_nr, Condition.operator == '>'),
                    and_(Condition.threshold > people_nr, Condition.operator == '<')))

    for condition in conditions:
        conditions_list.append(condition)
    return conditions_list


def update_last_occurred_time(id):
    master_db = get_master()
    condition = master_db.query(Condition).get(id)

    if not condition:
        abort(make_response('{"message": "Condition does not exist"}', 404))

    condition.last_occurred = datetime.datetime.now()
    master_db.commit()

    return {"message": "Success"}

