from service.email_service import send_email
from utils.condition_processor import get_action_receivers, get_webhook
import requests
import threading


def trigger_condition_action(image_processor_data, condition):
    for action in condition.actions:
        if action["type"] == "EMAIL":
            send_email(image_processor_data, condition)
        elif action["type"] == "SMS":
            print("------SMS NOTIFICATION-----")
            print(action["receivers"])
        elif action["type"] == "SOCIAL_MESSAGE":
            print("------SOCIAL_MESSAGE NOTIFICATION-----")
            print(action["receivers"])
        elif action["type"] == "WEBHOOK":
            request_webhook(image_processor_data, condition)


def request_webhook(image_processor_data, condition):
    processing_thread = threading.Thread(target=make_webhook_request, args=(image_processor_data, condition,))
    processing_thread.start()


def make_webhook_request(image_processor_data, condition):
    receivers = get_action_receivers(condition, "WEBHOOK")
    for webhook in receivers:
        url = get_webhook(image_processor_data, condition, webhook)
        requests.get(url)

