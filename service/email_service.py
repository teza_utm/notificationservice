import smtplib
import ssl
import threading
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from subscriber.message_processor import get_people_number
from utils.configuration import get_email_configs
from utils.condition_processor import get_action_receivers


sender_email, password = get_email_configs(filename='config/email_configs.ini')


def create_overcrowd_email(image_data, condition, receivers):
    email_content = f"""
        Hello!
        We are sorry to announce, but the area video monitored by camera {condition.camera_id} is overcrowded!
        There are {get_people_number(image_data)} visitors, but are allowed only {condition.threshold}!
        """
    email = MIMEMultipart()
    email['From'] = sender_email
    email['To'] = ", ".join(receivers)
    email['Subject'] = 'WARNING! Overcrowded Area'
    email.attach(MIMEText(email_content, 'plain'))

    return email


def create_underpopulated_email(image_data, condition, receivers):
    email_content = f"""
        Hello!
        We are sorry to announce, but the area video monitored by camera {condition.camera_id} has not enough employees!
        There are just {get_people_number(image_data)} employees, but are requested {condition.threshold}!
        """
    email = MIMEMultipart()
    email['From'] = sender_email
    email['To'] = ", ".join(receivers)
    email['Subject'] = 'WARNING! Lack of employees'
    email.attach(MIMEText(email_content, 'plain'))

    return email


def get_appropriate_email(condition):
    if condition.operator == '>':
        return create_overcrowd_email
    else:
        return create_underpopulated_email


def send_email_to_receivers(image_data, condition):
    receivers = get_action_receivers(condition, "EMAIL")
    create_email = get_appropriate_email(condition)
    email = create_email(image_data, condition, receivers)

    s = smtplib.SMTP('smtp.gmail.com:587')
    s.ehlo()
    s.starttls()
    s.login(sender_email, password)
    s.sendmail(sender_email, receivers, email.as_string())
    s.quit()

    # port = 465
    # context = ssl.create_default_context()
    # with smtplib.SMTP_SSL("smtp.gmail.com", port, context) as server:
    #     server.login(sender_email, password)
    #     server.sendmail(sender_email, receivers, email.as_string())


def send_email(image_processor_data, condition):
    processing_thread = threading.Thread(target=send_email_to_receivers, args=(image_processor_data, condition,))
    processing_thread.start()