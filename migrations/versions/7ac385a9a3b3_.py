"""empty message

Revision ID: 7ac385a9a3b3
Revises: e5e4ba0c1eac
Create Date: 2021-04-01 11:57:34.210540

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7ac385a9a3b3'
down_revision = 'e5e4ba0c1eac'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('condition', sa.Column('last_occurred', sa.DateTime(), nullable=True))
    op.add_column('condition', sa.Column('occurrence', sa.JSON(), nullable=False))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('condition', 'occurrence')
    op.drop_column('condition', 'last_occurred')
    # ### end Alembic commands ###
