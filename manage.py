from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from config.app_config import db, get_app

app = get_app()
manager = Manager(app)
migrate = Migrate(app, db, compare_type=True)
manager.add_command('db', MigrateCommand)

manager.run()