import os
import connexion
from config.db_replication_config import RouteSQLAlchemy

db = RouteSQLAlchemy()

user = os.environ["POSTGRES_USER"]
password = os.environ["POSTGRES_PASSWORD"]
database = os.environ["POSTGRES_DB"]
port = os.environ["POSTGRES_PORT"]
master_host = os.environ["POSTGRES_HOST_MASTER"]
slave_host = os.environ["POSTGRES_HOST_SLAVE"]


def get_app():
    global db
    connex_app = connexion.App(__name__, specification_dir='../')
    connex_app.add_api('notification.yml')
    app = connex_app.app
    config_app(app)
    db.init_app(app)

    return app


def config_app(app):
    SQLALCHEMY_DATABASE_URI = f"postgresql://{user}:{password}@{master_host}:{port}/{database}"
    app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
    app.config['SQLALCHEMY_BINDS'] = {
        'master': SQLALCHEMY_DATABASE_URI,
        'slave': f"postgresql://{user}:{password}@{slave_host}:{port}/{database}"
    }
    app.config['SQLALCHEMY_ENGINE_OPTIONS'] = {
        'connect_args': {
            'connect_timeout': 3
        }
    }
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
