from config.app_config import db


def get_master():
    db_connection = db.session
    if is_db_connection_available(db_connection):
        print("-------------------------------", flush=True)
        print("Current using master", flush=True)
        print("-------------------------------", flush=True)
        return db_connection
    else:
        db_connection.rollback()
        return get_slave()


def get_slave():
    db_connection = db.session.using_bind('slave')
    if is_db_connection_available(db_connection):
        print("-------------------------------", flush=True)
        print("Current using slave", flush=True)
        print("-------------------------------", flush=True)
        return db_connection
    else:
        return get_master()


def is_db_connection_available(db_connection):
    try:
        db_connection.execute("SELECT 1;")
        return True
    except Exception:
        return False
