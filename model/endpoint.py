class Endpoint:
    def __init__(self, path, method, cache_timeout):
        self.path = path
        self.method = method
        self.cache_timeout = cache_timeout
