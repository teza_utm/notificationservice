class Action:
    type = ""
    receivers = []

    def __init__(self, action_type, receivers):
        self.type = action_type
        self.receivers = receivers
