from config.app_config import db
from sqlalchemy import JSON
from model.action import Action


class Condition(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    camera_id = db.Column(db.String, nullable=False)
    name = db.Column(db.String, nullable=False)
    threshold = db.Column(db.Integer, nullable=False)
    operator = db.Column(db.String, nullable=False)
    actions = db.Column(JSON, nullable=False)
    occurrence = db.Column(JSON, nullable=False)
    last_occurred = db.Column(db.DateTime(), nullable=True)

    def __init__(self, camera_id, name, threshold, operator, actions, occurrence):
        self.camera_id = camera_id
        self.name = name
        self.threshold = threshold
        self.operator = operator
        self.actions = [Action(action_data["type"], action_data["receivers"]).__dict__ for action_data in actions]
        self.occurrence = occurrence

    def to_json(self):
        return {"id": self.id,
                "camera_id": self.camera_id,
                "name": self.name,
                "threshold": self.threshold,
                "operator": self.operator,
                "actions": self.actions,
                "occurrence": self.occurrence,
                "last_occurred": str(self.last_occurred)}
