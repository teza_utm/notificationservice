import enum


class ActionType(enum.Enum):
    SMS = 1
    EMAIL = 2
    SOCIAL_MESSAGE = 3
    WEBHOOK = 4
