from model.action_types import ActionType


def is_action_type_valid(request_payload):
    action_type_enum = tuple(item.name for item in ActionType)
    action_type = [value['type'] for value in request_payload["actions"]]

    return all(type_value in action_type_enum for type_value in action_type)
