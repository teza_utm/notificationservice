from subscriber.message_processor import get_people_number


def get_action_receivers(condition, action_type):
    receivers = []
    for action in condition.actions:
        if action["type"] == action_type:
            receivers = action["receivers"].split("; ")

    return receivers


def get_webhook(image_processor_data, condition, webhook):
    webhook = webhook.replace("{camId}", condition.camera_id)
    webhook = webhook.replace("{count}", str(get_people_number(image_processor_data)))
    webhook = webhook.replace("{limit}", str(condition.threshold))

    return webhook

