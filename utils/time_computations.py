import datetime
import math


def get_time_difference(condition):
    difference = 0
    time_unit = condition.occurrence["time_units"]
    delta_time = datetime.datetime.now() - condition.last_occurred
    total_sec = math.floor(delta_time.total_seconds())

    if time_unit == "SECOND":
        difference = total_sec
    elif time_unit == "MINUTE":
        difference = total_sec / 60
    elif time_unit == "HOUR":
        difference = total_sec / 3600
    elif time_unit == "DAY":
        difference = delta_time.days

    return difference
