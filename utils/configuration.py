import configparser


def get_email_configs(filename='config/email_configs.ini'):
    config = configparser.ConfigParser()
    config.read(filename)

    email = config["gmail_credentials"]["sender_email"]
    password = config["gmail_credentials"]["password"]

    return email, password


def get_broker_configs(key, field=None, filename='config/broker_configs.ini'):
    config = configparser.ConfigParser()
    config.read(filename)
    if field:
        return config[key][field]
    else:
        host = config[key]["host_rabbit"]
        port = config["RMQ_common"]["port"]
        user = config[key]["user"]
        password = config[key]["password"]
        virtual_host = config["RMQ_common"]["virtual_host"]
        return host, port, user, password, virtual_host