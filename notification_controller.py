import os
import json
from config.app_config import get_app
from flask import request, Response
from service.notification_service import get_all_conditions, create_condition, update_condition, delete_condition
from service.notification_service import get_condition_by_id
from subscriber.subscribtion import subscribe_to_broker
from proxy_connection import proxy_register


app = get_app()


@app.route('/conditions', methods=["GET"])
def list_all_conditions():
    return Response(json.dumps(get_all_conditions()), mimetype="application/json")


@app.route('/condition/<id>', methods=["GET"])
def get_condition(id):
    return Response(json.dumps(get_condition_by_id(id)), mimetype="application/json")


@app.route('/condition', methods=["POST"])
def create_new_condition():
    request_payload = json.loads(request.data)
    return Response(json.dumps(create_condition(request_payload)), mimetype="application/json")


@app.route('/condition/<id>', methods=["PUT"])
def modify_condition(id):
    request_payload = json.loads(request.data)
    return Response(json.dumps(update_condition(id, request_payload)), mimetype="application/json")


@app.route('/condition/<id>', methods=["DELETE"])
def remove_condition(id):
    return Response(json.dumps(delete_condition(id)), mimetype="application/json")


if __name__ == "__main__":
    proxy_register.register_service()
    subscribe_to_broker()
    app.run(host="0.0.0.0", port=int(os.environ["PORT"]), debug=True, threaded=True, use_reloader=False)