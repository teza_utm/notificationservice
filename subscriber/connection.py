import time
import pika
import os


def check_broker_connection(host, port, user, password, virtual_host):
    credentials = pika.PlainCredentials(user, password)
    parameters = pika.ConnectionParameters(host, port, virtual_host, credentials,
                                           heartbeat=600, blocked_connection_timeout=300)
    while True:
        try:
            connection = pika.BlockingConnection(parameters)
            return connection
        except pika.exceptions.AMQPConnectionError:
            print("AMQPConnectionError. Failed to connect to RabbitMQ-server. Waiting for activity..")
            time.sleep(1)
            continue
        except pika.adapters.utils.connection_workflow.AMQPConnectorSocketConnectError:
            print(f"RabbitMQ-server not found at {os.environ['HOST']}")
