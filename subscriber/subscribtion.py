import pika
import threading
import os
import json
from utils.configuration import get_broker_configs
from subscriber.connection import check_broker_connection
from subscriber.message_processor import get_exceeded_conditions
from service.action_service import trigger_condition_action
from service.notification_service import update_last_occurred_time
from config.app_config import get_app


def get_data_from_broker():
    app = get_app()
    app.app_context().push()
    key = os.environ["HOST"]
    host, port, user, password, virtual_host = get_broker_configs(key)

    exchange = "image_data"
    queue = ""

    print("Waiting for messages. To exit press CTRL+C")

    connection = check_broker_connection(host, port, user, password, virtual_host)
    channel = connection.channel()
    channel.exchange_declare(exchange=exchange,
                             exchange_type="fanout")

    result = channel.queue_declare(queue=queue,
                                   exclusive=True)
    queue_name = result.method.queue
    channel.queue_bind(exchange=exchange,
                       queue=queue_name)

    def callback(ch, method_frame, properties, data):
        try:
            print(method_frame, "*" * 100, flush=True)
            if method_frame:
                print("Consumed message: ", data.decode(), queue, flush=True)
                image_processor_data = json.loads(data)
                exceeded_conditions = get_exceeded_conditions(image_processor_data)

                print("--------FOUND CONDITIONS-------", flush=True)
                print(exceeded_conditions, flush=True)
                print("------------------------", flush=True)

                for condition in exceeded_conditions:
                    trigger_condition_action(image_processor_data, condition)
                    update_last_occurred_time(condition.id)
            else:
                print("Method frame: None. Invalid message in queue", flush=True)
        except pika.exceptions.ConnectionClosedByBroker:
            print("Connection closed by broker. Lost message: ", data, flush=True)

    # channel.basic_qos(prefetch_count=1)
    channel.basic_consume(queue=queue_name,
                          on_message_callback=callback,
                          auto_ack=True)
    channel.start_consuming()


def subscribe_to_broker():
    subscribing_thread = threading.Thread(target=get_data_from_broker)
    subscribing_thread.start()