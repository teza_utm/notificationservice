from service.notification_service import get_conditions_by_camera_id_threshold
from utils.time_computations import get_time_difference


def get_people_number(message):
    people_data = message["people"]
    return len(people_data)


def get_exceeded_conditions(message):
    exceeded_conditions = []
    people_nr = get_people_number(message)
    conditions = get_conditions_by_camera_id_threshold(message["camId"], people_nr)

    for condition in conditions:
        if condition.last_occurred:
            time_difference = get_time_difference(condition)
            if time_difference > condition.occurrence["time"]:
                exceeded_conditions.append(condition)
        else:
            exceeded_conditions.append(condition)

    return exceeded_conditions
